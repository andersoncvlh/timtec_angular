angular.module('app').controller('AppController',
	function($scope, $filter) {
		$scope.nome = "Anderson";
		var nome = "bla bla";



		$scope.alunos = [
			{'nome':'João','idade':9},
			{'nome':'Maria','idade':12},
			{'nome':'Lucas','idade':15},
			{'nome':'Anderson','idade':23},
			{'nome':'Felipe','idade':8},
			{'nome':'Carol','idade':17}
		]
		$scope.finalizar = function () {
			// body...
			$scope.iniciado = true;
		}

		$scope.comecar = function () {
			$scope.iniciado = false;
		}

		$scope.hoje = new Date();

		$scope.cadastrar = function() {
			if ($scope.form_1.$valid) {
			novo_aluno = {};
	        novo_aluno['nome'] = $scope.nome_aluno;
	        novo_aluno['idade'] = parseInt($scope.idade_aluno);

	        $scope.alunos.push(novo_aluno);
			} else {
				alert('eita');
			}
		}

		$scope.ordernarPorNome = function () {
			$scope.ordernadoPorNome = !$scope.ordernadoPorNome;
			$scope.alunos = $filter('orderBy')($scope.alunos, 'nome', $scope.ordernadoPorNome);
		}

		$scope.ordernarPorIdade = function () {
			$scope.ordernadoPorIdade = !$scope.ordernadoPorIdade;
			$scope.alunos = $filter('orderBy')($scope.alunos, 'idade', $scope.ordernadoPorIdade);
		}
	}
);