angular.module('app').controller('PrevisaoController',
    function ($scope,$resource) {
        // body...
        var WeatherChannel = $resource('http://api.openweathermap.org/data/2.5/weather?q=SaoPaulo,BR&units=metric&APPID=4bebe5869e4a87819e3ceabdd0ee9113');
        var getWeather = function () {
            WeatherChannel.get().$promise.then(function(response) {
                $scope.tempo = response;
                console.log(response);
            }, function(promise) {
                alert("Erro ao requisitar Recurso!");
            })
        }

        getWeather();

    }
);